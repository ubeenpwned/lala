$action = New-ScheduledTaskAction -Execute 'c:\Windows\System32\WindowsPowershell\v1.0\powershell.exe' "C:\Windows\mail.ps1"
$trigger = New-ScheduledTaskTrigger -Once -At 4am
$principal = New-ScheduledTaskPrincipal -UserID "NT AUTHORITY\SYSTEM" -LogonType S4U -RunLevel Highest
$task = Register-ScheduledTask -TaskName "salut" -Trigger $trigger -Action $action -Principal $principal
$task.Triggers.Repetition.Duration = "P1D" 
$task.Triggers.Repetition.Interval = "PT1M" 
$task.Settings.Hidden = $true
$task | Set-ScheduledTask
